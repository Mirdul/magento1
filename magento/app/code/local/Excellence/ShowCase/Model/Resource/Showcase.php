<?php
class Excellence_ShowCase_Model_Resource_Showcase extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('showcase/showcase', 'id');
    }
}
