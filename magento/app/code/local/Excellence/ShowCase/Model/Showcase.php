<?php
class Excellence_ShowCase_Model_Showcase extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('showcase/showcase');
    }
}
