<?php
class Excellence_ShowCase_Model_Config_Source_Price
{
    public function toOptionArray()
    {
        return array(
            array('value' => '', 'label' => Mage::helper('adminhtml')->__('Select')),
            array('value' => 'below_200', 'label' => Mage::helper('adminhtml')->__('below_200')),
            array('value' => 'below_400', 'label' => Mage::helper('adminhtml')->__('below_400')),
            array('value' => 'below_600', 'label' => Mage::helper('adminhtml')->__('below_600')),
            array('value' => 'below_800', 'label' => Mage::helper('adminhtml')->__('below_800')),
        );
    }
}
