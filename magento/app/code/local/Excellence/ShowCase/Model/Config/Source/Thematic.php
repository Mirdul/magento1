<?php
class Excellence_ShowCase_Model_Config_Source_Thematic
{
    public function toOptionArray()
    {
        return array(
            array('value' => '', 'label' => Mage::helper('adminhtml')->__('Select')),
            array('value' => 'Independência', 'label' => Mage::helper('adminhtml')->__('Independência')),
            array('value' => 'Dia das Crianças', 'label' => Mage::helper('adminhtml')->__('Dia das Crianças')),
            array('value' => 'Finados', 'label' => Mage::helper('adminhtml')->__('Finados')),
            array('value' => 'Proclamação da República', 'label' => Mage::helper('adminhtml')->__('Proclamação da República')),
        );
    }
}
