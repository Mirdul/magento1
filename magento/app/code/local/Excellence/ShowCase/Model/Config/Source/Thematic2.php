<?php
class Excellence_ShowCase_Model_Config_Source_Thematic2
{
    public function toOptionArray()
    {
        return array(
            array('value' => '', 'label' => Mage::helper('adminhtml')->__('Select')),
            array('value' => 'Premiados Zarpo', 'label' => Mage::helper('adminhtml')->__('Premiados Zarpo')),
            array('value' => 'Novas Ofertas', 'label' => Mage::helper('adminhtml')->__('Novas Ofertas')),
            array('value' => 'Viajar a Dois', 'label' => Mage::helper('adminhtml')->__('Viajar a Dois')),
            array('value' => 'Viajar com Crianças', 'label' => Mage::helper('adminhtml')->__('Viajar com Crianças')),
        );
    }
}