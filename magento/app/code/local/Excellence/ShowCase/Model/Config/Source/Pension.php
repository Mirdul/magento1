<?php
class Excellence_ShowCase_Model_Config_Source_Pension
{
    public function toOptionArray()
    {
        return array(
            array('value' => '', 'label' => Mage::helper('adminhtml')->__('Select')),
            array('value' => 'All-Inclusive', 'label' => Mage::helper('adminhtml')->__('All-Inclusive')),
            array('value' => 'Pensão Completa', 'label' => Mage::helper('adminhtml')->__('Pensão Completa')),
            array('value' => 'Meia Pensão', 'label' => Mage::helper('adminhtml')->__('Meia Pensão')),
            array('value' => 'Café da Manhã', 'label' => Mage::helper('adminhtml')->__('Café da Manhã')),
        );
    }
}