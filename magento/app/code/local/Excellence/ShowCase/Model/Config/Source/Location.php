<?php
class Excellence_ShowCase_Model_Config_Source_Location
{
    public function toOptionArray()
    {
        return array(
            array('value' => '', 'label' => Mage::helper('adminhtml')->__('Select')),
            array('value' => 'Espírito Santo', 'label' => Mage::helper('adminhtml')->__('Espírito Santo')),
            array('value' => 'Minas Gerais', 'label' => Mage::helper('adminhtml')->__('Minas Gerais')),
            array('value' => 'Rio de Janeiro', 'label' => Mage::helper('adminhtml')->__('Rio de Janeiro')),
            array('value' => 'São Paulo ', 'label' => Mage::helper('adminhtml')->__('São Paulo')),
        );
    }
}