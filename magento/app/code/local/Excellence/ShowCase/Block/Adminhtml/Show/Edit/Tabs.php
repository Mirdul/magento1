<?php
class Excellence_ShowCase_Block_Adminhtml_Show_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('showcase_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('showcase')->__('CSV Information'));
    }

    protected function _beforeToHtml()
    {

        $this->addTab('showcase_section', array(
            'label' => Mage::helper('showcase')->__('CSV'),
            'title' => Mage::helper('showcase')->__('CSV File'),
            'content' => $this->getLayout()->createBlock('showcase/adminhtml_show_edit_tab_form')->toHtml(),
        ));

        return parent::_beforeToHtml();
    }
}
