<?php
class Excellence_ShowCase_Block_Adminhtml_Show_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {

        parent::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'showcase';
        $this->_controller = 'adminhtml_show';

        $this->_updateButton('save', 'label', Mage::helper('showcase')->__('Save'));
        $this->_addButton('saveandcontinue', array(
            'label' => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick' => 'saveAndContinueEdit()',
            'class' => 'save',
        ), -100);
        $this->_formScripts[] = "

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";

    }

    public function getHeaderText()
    {
        return Mage::helper('showcase')->__('show case');
    }

}
