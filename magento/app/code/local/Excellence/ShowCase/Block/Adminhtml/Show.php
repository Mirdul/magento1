<?php
class Excellence_ShowCase_Block_Adminhtml_Show extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'showcase';
        $this->_controller = 'adminhtml_show';
        $this->_headerText = $this->__('Grid');
        parent::__construct();
    }
}
