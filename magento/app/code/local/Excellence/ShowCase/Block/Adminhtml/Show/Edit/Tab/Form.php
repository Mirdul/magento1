<?php
class Excellence_ShowCase_Block_Adminhtml_Show_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('showcase/show.phtml');
    }
     public function getCurrentData() 
    {
        return Mage::registry('showcase_data');
    }
}
