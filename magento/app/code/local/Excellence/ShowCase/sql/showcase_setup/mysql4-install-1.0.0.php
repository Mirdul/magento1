
<?php
$installer = $this;
$installer->startSetup();
$installer->run("
-- DROP TABLE IF EXISTS {$this->getTable('showcase')};
CREATE TABLE {$this->getTable('showcase')} (
  `id` int(11) unsigned NOT NULL auto_increment,
  `show_name` varchar(5000) NOT NULL default 'showcase',
  `show_value` varchar(5000) NOT NULL default '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
$installer->endSetup();

?>