<?php
class Excellence_ShowCase_Adminhtml_ShowController extends Mage_Adminhtml_Controller_Action
{

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('showcase/show');
    }

    public function indexAction()
    {
        $this->loadLayout();
        $this->_title($this->__("Admin Grid"));
        $this->_setActiveMenu('showcase/show');
        $this->renderLayout();
    }

    public function saveAction()
    {

        $redirectBack = $this->getRequest()->getParam('back', false);
        $data = $this->getRequest()->getPost();
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('showcase/showcase')->load($id);
        $preData = unserialize($model['show_value']);
        if (isset($_FILES['image']['name']) && $_FILES['image']['name'] != '') {
            $total_files = count($_FILES['image']['name']);
            for ($i = 1; $i <= $total_files; $i++) {
                if ($preData['image'][$i] != '') {
                    $data['image'][$i] = $preData['image'][$i];
                }

                if ($preData['image'][$i] != '' || $_FILES['image']['name'][$i] != '') {
                    try {
                        $uploader = new Varien_File_Uploader('image[' . $i . ']');
                        $fileinfo = $uploader;

                        $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
                        $uploader->setAllowRenameFiles(false);
                        $uploader->setFilesDispersion(false);

                        $path = Mage::getBaseDir('media') . DS . 'showcase' . DS;
                        $uploader->save($path, $_FILES['image']['name'][$i]);
                        $fileinfo = getimagesize($path . $_FILES['image']['name'][$i]);
                        $width = $fileinfo[0];
                        $height = $fileinfo[1];
                        if ($width != 964 && $height != 360) {
                            Mage::getSingleton('adminhtml/session')->_redirect('*/*/edit', array(
                                'id' => $this->getRequest()->getParam('id'),
                                '_current' => true,
                            ));

                        }
                        $data['image'][$i] = $_FILES['image']['name'][$i];

                    } catch (Exception $e) {

                    }
                } else {
                    unset($data['location'][$i]);
                    unset($data['thematic'][$i]);
                    unset($data['thematic2'][$i]);
                    unset($data['pension'][$i]);
                    unset($data['price'][$i]);
                }

            }#end for loop
            $data = serialize($data);
            $model = Mage::getModel('showcase/showcase');
            $model->setData($data)
                ->setId($this->getRequest()->getParam('id'));
            $model->setShowValue($data);
            $model->save();
            $id = $model->getId();
            if ($redirectBack) {
                $this->_redirect('*/*/edit', array(
                    'id' => $id,
                    '_current' => true,
                ));
            } else {
                $this->_redirect('*/*/');
            }
        }#end if
    }
    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('showcase/showcase')->load($id);
        if ($model->getId() || $id == 0) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data)) {
                $model->setData($data);
            }
            Mage::register('showcase_data', $model);
            $this->loadLayout();
            $this->_setActiveMenu('showcase/show');
            $this->_addContent($this->getLayout()->createBlock('showcase/adminhtml_show_edit'))
                ->_addLeft($this->getLayout()->createBlock('showcase/adminhtml_show_edit_tabs'));
            $this->renderLayout();

        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('showcase')->__('data not save'));
            $this->_redirect('*/*/');
        }

    }
}
