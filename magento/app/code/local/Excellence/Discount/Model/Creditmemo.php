<?php
class Excellence_Discount_Model_Creditmemo extends Mage_Sales_Model_Order_Creditmemo_Total_Abstract
{
    public function collect(Mage_Sales_Model_Order_Creditmemo $creditmemo)
    {
        $amount = Mage::getStoreConfig('adminhtml/sales/order');
        $creditmemo->setGrandTotal($creditmemo->getGrandTotal() - $amount);
        $creditmemo->setBaseGrandTotal($creditmemo->getBaseGrandTotal() - $amount);

        return $this;
    }
}
