<?php
class Excellence_Discount_Model_Discount extends Mage_Sales_Model_Quote_Address_Total_Abstract
{

    public function collect(Mage_Sales_Model_Quote_Address $address)
    {

        parent::collect($address);

        $this->_setAmount(0);
        $this->_setBaseAmount(0);

        $items = $this->_getAddressItems($address);
        if (!count($items)) {
            return $this;
        }

        $quote = $address->getQuote();

        $exist_amount = $quote->getFeeAmount();
        $fee = 15;
        $balance = $fee - $exist_amount;
        $address->setFeeAmount($balance);
        $address->setBaseFeeAmount($balance);

        $quote->setFeeAmount($balance);

        $address->setGrandTotal($address->getGrandTotal() - $address->getFeeAmount());
        $address->setBaseGrandTotal($address->getBaseGrandTotal() - $address->getBaseFeeAmount());

    }

    public function fetch(Mage_Sales_Model_Quote_Address $address)
    {

        $amount = $address->getFeeAmount();
        if ($amount != 0) {
            Mage::getConfig()->saveConfig('adminhtml/sales/order', $amount, 'default', 0);
            $address->addTotal(array(
                'code' => $this->getCode(),
                'title' => Mage::helper('discount')->__('Discount(-)'),
                'value' => $amount,
            ));

        }

        return $address;
    }

}
