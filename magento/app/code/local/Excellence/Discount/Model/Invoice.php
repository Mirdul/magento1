<?php 
class Excellence_Discount_Model_Invoice extends Mage_Sales_Model_Order_Invoice_Total_Abstract
{ 
    public function collect(Mage_Sales_Model_Order_Invoice $invoice)
    {
        $amount=Mage::getStoreConfig('adminhtml/sales/order');
        $invoice->setGrandTotal($invoice->getGrandTotal() - $amount);
        $invoice->setBaseGrandTotal($invoice->getBaseGrandTotal() - $amount);

        return $this;
    }
}
