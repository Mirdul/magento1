<?php
class Excellence_Discount_Block_Adminhtml_Sales_Order extends Mage_Sales_Block_Order_Totals
{

    protected function _initTotals()
    {
        parent::_initTotals();
        $amount = Mage::getStoreConfig('adminhtml/sales/order');
        $baseAmount = $this->getSource()->getBaseFeeAmount();
        if ($amount != 0) {
            $this->addTotal(new Varien_Object(array(
                'code' => 'Fee',
                'value' => $amount,
                'label' => 'Discount(-)',
            )), 'discount');
        }
        return $this;
    }
}
