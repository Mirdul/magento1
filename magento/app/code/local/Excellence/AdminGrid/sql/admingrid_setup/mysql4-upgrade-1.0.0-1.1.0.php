<?php
$installer = $this;
$installer->startSetup();
$installer->run("

-- DROP TABLE IF EXISTS {$this->getTable('csvfile')};
CREATE TABLE {$this->gettable('csvfile')} (
  `csvfile_id` int(11) unsigned NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  `date` int(11) unsigned NOT NULL default '0' ,
  `mstay` int(11) unsigned NOT NULL default '0' ,
  `pcheckout` int(11) unsigned NOT NULL default '0' ,
  `savailable` int(11) unsigned NOT NULL default '0' ,
  `rprice` int(11) unsigned NOT NULL default '0' ,
  `zprice` int(11) unsigned NOT NULL default '0' ,
  `cprice` int(11) unsigned NOT NULL default '0' ,
  PRIMARY KEY(`csvfile_id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
$installer->endSetup();
