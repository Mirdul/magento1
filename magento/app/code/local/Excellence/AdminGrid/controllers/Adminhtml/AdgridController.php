<?php
class Excellence_AdminGrid_Adminhtml_AdgridController extends Mage_Adminhtml_Controller_Action
{

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admingrid/adgrid');
    }

    public function indexAction()
    {
        $this->loadLayout();
        $this->_title($this->__("Admin Grid"));
        $this->_setActiveMenu('admingrid/adgrid');
        $this->renderLayout();
    }
    public function newAction()
    {
        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('admingrid/adminhtml_adgrid_edit'))
            ->_addLeft($this->getLayout()->createBlock('admingrid/adminhtml_adgrid_edit_tabs'));
        $this->renderLayout();
        $this->_setActiveMenu('admingrid/adgrid');
    }
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            $redirectBack = $this->getRequest()->getParam('back', false);
            $model = Mage::getModel('admingrid/csvfile');
            $model->setData($data)
                ->setId($this->getRequest()->getParam('id'));
            $model->setName($data['name']);
            $model->setDate($data['date']);
            $model->setMstay($data['mstay']);
            $model->setPcheckout($data['pcheckout']);
            $model->setSavailable($data['savailable']);
            $model->setRprice($data['rprice']);
            $model->setZprice($data['zprice']);
            $model->setCprice($data['cprice']);
            $model->save();
            $id = $model->getcsvfileId();
            if ($redirectBack) {
                $this->_redirect('*/*/edit', array(
                    'id' => $id,
                    '_current' => true,
                ));
            } else {
                $this->_redirect('*/*/');
            }

        }
    }
    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('admingrid/csvfile')->load($id);
        if ($model->getId() || $id == 0) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data)) {
                $model->setData($data);
            }

            Mage::register('csvfile_data', $model);

            $this->loadLayout();
            $this->_setActiveMenu('admingrid/adgrid');
            $this->_addContent($this->getLayout()->createBlock('admingrid/adminhtml_adgrid_edit'))
                ->_addLeft($this->getLayout()->createBlock('admingrid/adminhtml_adgrid_edit_tabs'));
            $this->renderLayout();

        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('admingrid')->__('data not save'));
            $this->_redirect('*/*/');
        }

    }
    public function deleteAction()
    {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $model = Mage::getModel('admingrid/csvfile');
                $model->setId($this->getRequest()->getParam('id'))->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('File was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }

        $this->_redirect('*/*/');
    }

    public function downloadXmlAction()
    {
        Mage::helper('admingrid/data')->downloadXml();

    }

}
