<?php
class Excellence_AdminGrid_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function downloadXml()
    {
        header('Content-Disposition: attachment; filename="xmlfile.xml"');
        $count = 0;
        $xml_order_info = new \SimpleXMLElement("<?xml version=\"1.0\" encoding=\"Windows-1252\"?><orders></orders>");
        $orders = Mage::getModel('sales/order')->getCollection()->getData();
        foreach ($orders as $orderItem) {
            $orderdata = Mage::getModel('sales/order')->load($orderItem['entity_id']);
            $orderItems = $orderdata->getItemsCollection();
            $id = $orderItem['increment_id'];
            $name = $orderItem['customer_firstname'] . ' ' . $orderItem['customer_lastname'];
            $order = $xml_order_info->addChild('order');
            $order->addAttribute('id', $id);
            $order->addChild('customer_name', $name);
            $items = $order->addChild('items');
            foreach ($orderItems as $key => $product) {
                $count += 1;
                $item = $items->addChild('item');
                $item->addAttribute('number', $count);
                $item->addChild('item_id', $product->getproductId());
                $item->addChild('item_name', $product->getname());
                $item->addChild('item_price', $product->getprice());
            }
            $count = 0;

        }

        $xml_file = $xml_order_info->asXML();
        print_r($xml_file);

    }
}
