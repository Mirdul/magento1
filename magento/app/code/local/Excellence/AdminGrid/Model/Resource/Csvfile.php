<?php
class Excellence_AdminGrid_Model_Resource_Csvfile extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('admingrid/csvfile', 'csvfile_id');
    }
}
