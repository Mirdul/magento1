<?php
class Excellence_AdminGrid_Block_Adminhtml_Adgrid_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    public function __construct()
    {  
        parent::__construct();
        $this->setId('admingrid_tabs');
        $this->setDestElementId('edit_admingrid'); // this should be same as the form id define above
        $this->setTitle(Mage::helper('admingrid')->__('CSV Information'));
    }

    protected function _beforeToHtml()
    {

        $this->addTab('admingrid_section', array(
            'label' => Mage::helper('admingrid')->__('CSV'),
            'title' => Mage::helper('admingrid')->__('Item Information'),
            'content' => $this->getLayout()->createBlock('admingrid/adminhtml_adgrid_edit_tab_form')->toHtml(),
        ));

        return parent::_beforeToHtml();
    }
}
