<?php
class Excellence_AdminGrid_Block_Adminhtml_Adgrid_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('admingrid_form', array('legend' => Mage::helper('admingrid')->__('CSV File')));

        $fieldset->addField('name', 'text', array(
            'label' => Mage::helper('admingrid')->__('File Name'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'name',
        ));

        $fieldset->addField('date', 'text', array(
            'label' => Mage::helper('admingrid')->__('Date'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'date',
        ));
        $fieldset->addField('mstay', 'text', array(
            'label' => Mage::helper('admingrid')->__('Min.Stay'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'mstay',
        ));
        $fieldset->addField('pcheckout', 'text', array(
            'label' => Mage::helper('admingrid')->__('Possible Checkout'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'pcheckout',
        ));
        $fieldset->addField('savailable', 'text', array(
            'label' => Mage::helper('admingrid')->__('Stock Available'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'savailable',
        ));
        $fieldset->addField('rprice', 'text', array(
            'label' => Mage::helper('admingrid')->__('Reference Price'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'rprice',
        ));
        $fieldset->addField('zprice', 'text', array(
            'label' => Mage::helper('admingrid')->__('Zarpo Price'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'zprice',
        ));
        $fieldset->addField('cprice', 'text', array(
            'label' => Mage::helper('admingrid')->__('Client price'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'cprice',
        ));
        if ($commerciauxData = Mage::registry('csvfile_data')) {
            $form->setValues($commerciauxData->getData());
        }

        return parent::_prepareForm();
    }
}
