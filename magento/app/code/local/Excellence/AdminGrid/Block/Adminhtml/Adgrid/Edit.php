<?php 
class Excellence_AdminGrid_Block_Adminhtml_Adgrid_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{ 
    public function __construct()
    {
        
        parent::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'admingrid';
        $this->_controller = 'adminhtml_adgrid';

        $this->_updateButton('save', 'label', Mage::helper('admingrid')->__('Save'));
        $this->_updateButton('delete', 'label', Mage::helper('admingrid')->__('Delete'));

        $this->_addButton('saveandcontinue', array(
            'label' => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick' => 'saveAndContinueEdit()',
            'class' => 'save',
        ), -100);
        $this->_formScripts[] = "

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";

    }

    public function getHeaderText()
    {
        return Mage::helper('admingrid')->__('CSV Form');
    }

}
