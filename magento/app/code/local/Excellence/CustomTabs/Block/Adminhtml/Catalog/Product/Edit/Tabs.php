<?php
class Excellence_CustomTabs_Block_Adminhtml_Catalog_Product_Edit_Tabs extends Mage_Adminhtml_Block_Catalog_Product_Edit_Tabs
{
    /**
     * @var
     */
    private $parent;
    /**
     * @return mixed Mage_Core_Block_Abstract
     */
    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label' => Mage::helper('customtabs')->__('Csv File'),
            'title' => Mage::helper('customtabs')->__('Item Information'),
            'content' => $this->getLayout()->createBlock('customtabs/adminhtml_form_edit_tab_form')->toHtml(),
        ));

        return parent::_beforeToHtml();
    }
}
