<?php
class Excellence_CustomTabs_Block_Adminhtml_Form_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        // $form = new Varien_Data_Form();

        $form = new Varien_Data_Form(array(
            'id' => 'csv_form',
            'action' => $this->getUrl('*/*/validate'),
            'method' => 'post',
            'enctype' => 'multipart/form-data',
        ));

        $this->setForm($form);
        $fieldset = $form->addFieldset('customtabs_form', array('legend' => Mage::helper('catalog')->__('Csv File')));

        $fieldset->addField('simple_product_csv_file', 'file', array(
            'name' => 'csv',
            'label' => Mage::helper('customtabs')->__('Select File to Import'),
            'title' => Mage::helper('customtabs')->__('Select File to Import'),
            'required' => false,
        ));

        return parent::_prepareForm();
    }
}
