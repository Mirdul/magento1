<?php
$installer = $this;
$installer->startSetup();
$installer->run("

-- DROP TABLE IF EXISTS {$this->getTable('csv')};
CREATE TABLE {$this->gettable('csv')} (
  `pcsv_id` int(11) unsigned NOT NULL auto_increment,
  `date` datetime NULL,
  `min_stay` int(11) unsigned NOT NULL,
  `possible_checkout` int(11) unsigned NOT NULL,
  `stock_available` int(11) unsigned NOT NULL,
  `reference_price` int(11) unsigned NOT NULL,
  `zarpo_price` int(11) unsigned NOT NULL,
  `client_price` int(11) unsigned NOT NULL,
  PRIMARY KEY  (`pcsv_id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
$installer->endSetup();
