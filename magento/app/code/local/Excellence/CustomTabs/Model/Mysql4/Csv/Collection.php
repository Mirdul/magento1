<?php
class Excellence_CustomTabs_Model_Mysql4_Csv_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('customtabs/csv');

    }
}
