<?php
include_once "Mage/Adminhtml/controllers/Catalog/ProductController.php";
class Excellence_CustomTabs_Catalog_ProductController extends Mage_Adminhtml_Catalog_ProductController
{
    public function saveAction()
    {
        $storeId = $this->getRequest()->getParam('store');
        $redirectBack = $this->getRequest()->getParam('back', false);
        $productId = $this->getRequest()->getParam('id');
        $isEdit = (int) ($this->getRequest()->getParam('id') != null);
        $data = $this->getRequest()->getPost();
        $collection = Mage::getModel('admingrid/csvfile')->getCollection();
        $csvfile = $collection->getLastItem()->getdata();

        if ($data) {
            $this->_filterStockData($data['product']['stock_data']);

            $product = $this->_initProductSave();

            try {
                $product->save();
                $productId = $product->getId();

                if (isset($data['copy_to_stores'])) {
                    $this->_copyAttributesBetweenStores($data['copy_to_stores'], $product);
                }

                $this->_getSession()->addSuccess($this->__('The product has been saved.'));
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage())
                    ->setProductData($data);
                $redirectBack = true;
            } catch (Exception $e) {
                Mage::logException($e);
                $this->_getSession()->addError($e->getMessage());
                $redirectBack = true;
            }
        }
        if (isset($_FILES['csv']['name']) and (file_exists($_FILES['csv']['tmp_name']))) {
            try {
                $uploader = new Varien_File_Uploader('csv');
                $uploader->setAllowedExtensions(array('csv'));
                $uploader->setAllowRenameFiles(false);
                $path = Mage::getBaseDir('media') . DS;
                $uploader->save($path, $_FILES['csv']['name']);
                $data['csv'] = $_FILES['csv']['name'];
                $filen = 'media' . DS . $data["csv"];
                $csvObject = new Varien_File_Csv();
                $a = $csvObject->getData($filen);

                foreach ($a as $key => $data) {
                    if ($key!= 0) {
                        $model = Mage::getModel('customtabs/csv');
                        $model->setDate($data[$csvfile['date'] - 1]);
                        $model->setMinStay($data[$csvfile['mstay'] - 1]);
                        $model->setPossibleCheckout($data[$csvfile['pcheckout'] - 1]);
                        $model->setStockAvailable($data[$csvfile['savailable'] - 1]);
                        $model->setReferencePrice($data[$csvfile['rprice'] - 1]);
                        $model->setZarpoPrice($data[$csvfile['zprice'] - 1]);
                        $model->setClientPrice($data[$csvfile['cprice'] - 1]);
                        $model->save();

                    }
                }
            } catch (Exception $e) {
            }
        }
        $this->_redirect('*/*/', array('store' => $storeId));

        if ($redirectBack) {
            $this->_redirect('*/*/edit', array(
                'id' => $productId,
                '_current' => true,
            ));
        } elseif ($this->getRequest()->getParam('popup')) {
            $this->_redirect('*/*/created', array(
                '_current' => true,
                'id' => $productId,
                'edit' => $isEdit,
            ));
        } else {
            $this->_redirect('*/*/', array('store' => $storeId));
        }

    }
}
