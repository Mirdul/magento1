<?php
class Excellence_Test_Block_Test extends Mage_Core_Block_Template
{
    public function getContent()
    {
        $collection = Mage::getModel('test/test')->getCollection();
        $collection->getSelect()->join(array('emp'=>$collection->getTable('test/emp')),'main_table.test_id=emp.test_id',array('emp.emp_post'));
        $arrData = $collection->getData();
        return $arrData ;
    }
    
}
?>