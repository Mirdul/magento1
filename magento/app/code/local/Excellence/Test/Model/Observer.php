<?php
class Excellence_Test_Model_Observer
{
    public function saveOrderAfter($evt)
    {
        $order = $evt->getOrder(); //this is how we access data passed in event
        $quote = $evt->getQuote(); //this is how we access data passed in event
        $ord = $evt->getEvent()->getData("order");
        // print_r($ord);
    }
    public function customerLogin($observer)
    {
        if (Mage::getSingleton('customer/session')->isLoggedIn()) {
            $customerData = Mage::getSingleton('customer/session')->getCustomer();
            $customer_id = $customerData->getId();
            $name = $customerData->getName();

        }
        $table = Mage::getModel('test/user')->load($customer_id);
        $t_name = $table->getName();
        if (empty($t_name)) {
            $model = Mage::getModel('test/user');
            $model->setCustomerId($customer_id);
            $model->setname($name);
            $model->setLoginTime(strtotime('now'));
            $model->save();
        } else {
            $table->setLoginTime(strtotime('now'));
            $table->save();
        }

    }
    public function customerLogout($observer)
    {
        $customer_id = Mage::getSingleton('customer/session')->getId();
        $table = Mage::getModel('test/user')->load($customer_id);
        $table->setLogoutTime(strtotime('now'));
        $table->save();

    }

    public function setShipping($evt)
    {
        $controller = $evt->getControllerAction();
        if (!Mage::getSingleton('checkout/type_onepage')->getQuote()->getShippingAddress()->getCountryId() && Mage::getSingleton('checkout/type_onepage')->getQuote()->getItemsCount()) {
            $country_id = 'IN';
            $region_id = false;
            $country = Mage::getModel('directory/country')->loadByCode($country_id);
            $regions = $country->getRegions();
            if (sizeof($regions) > 0) {
                $region = $regions->getFirstItem();
                $region_id = $region->getId();
            }
            $customerSession = Mage::getSingleton("customer/session");
            if ($customerSession->isLoggedIn()) {
                $customerAddress = $customerSession->getCustomer()->getDefaultShippingAddress();
                if ($customerAddress->getId()) {
                    $customerCountry = $customerAddress->getCountryId();
                    $region_id = $customerAddress->getRegionId();
                    $region = $customerAddress->getRegion();
                    $quote = Mage::getSingleton('checkout/type_onepage')->getQuote();
                    $shipping = $quote->getShippingAddress();
                    $shipping->setCountryId($customerCountry);
                    if ($region_id) {
                        $shipping->setRegionId($region_id);
                    }
                    if ($region) {
                        $shipping->setRegion($region);
                    }
                    $quote->setShippingMethod('flatrate_flatrate');
                    $quote->save();
                    $controller->getResponse()->setRedirect(Mage::getUrl('*/*/*', array('_current' => true)));
                } else {
                    $quote = Mage::getSingleton('checkout/type_onepage')->getQuote();
                    $shipping = $quote->getShippingAddress();
                    $shipping->setCountryId($country_id);
                    if ($region_id) {
                        $shipping->setRegionId($region_id);
                    }
                    $quote->setShippingMethod('flatrate_flatrate');
                    $quote->save();
                    $controller->getResponse()->setRedirect(Mage::getUrl('*/*/*', array('_current' => true)));
                }
            } else {
                $quote = Mage::getSingleton('checkout/type_onepage')->getQuote();
                $shipping = $quote->getShippingAddress();
                $shipping->setCountryId($country_id);
                if ($region_id) {
                    $shipping->setRegionId($region_id);
                }
                $quote->setShippingMethod('flatrate_flatrate');
                $quote->save();
                $controller->getResponse()->setRedirect(Mage::getUrl('*/*/*', array('_current' => true)));
            }
        }

    }
}
