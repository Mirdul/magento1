<?php
class Excellence_Test_Model_User extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('test/user');
    }
}
