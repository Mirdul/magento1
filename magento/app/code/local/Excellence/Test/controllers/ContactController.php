<?php
class Excellence_Test_ContactController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }
    public function sendMailAction(){
        // store information in database.
        $data = $this->getRequest()->getPost();
        $model = Mage::getModel('test/contact');
        $model->setName($data['name']);
        $model->setEmail($data['email']);
        $model->setComment($data['comment']);
        $model->save();
        //mail send.
        $mail = Mage::getModel('core/email');
        $mail->setToName($data['name']);
        $mail->setToEmail($data['email']);
        $mail->setBody($data['comment']);
        $mail->setSubject('Not Reply');
        $mail->setFromName('MAGENTO SMTP');
        try {
        $mail->send();
        Mage::getSingleton('core/session')->addSuccess('Your request has been sent');
        $this->_redirect('test/contact');
        }
        catch (Exception $e) {
        Mage::getSingleton('core/session')->addError('Unable to send.');
        $this->_redirect('test/contact');
        }
    }
}
