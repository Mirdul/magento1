<?php
class Excellence_Test_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();

    }
    public function formAction()
    {
        $this->loadLayout();
        $this->renderLayout();

    }
    public function saveAction()
    {
        $data = $this->getRequest()->getPost();
        $model = Mage::getModel('test/test');
        $model->setFname($data['firstname']);
        $model->setLname($data['lastname']);
        $model->setEmail($data['useremail']);
        $model->setMobile($data['mobile']);
        $model->setStatus(1);
        $model->setCreatedTime(strtotime('now'));
        $model->setUpdatedTime(strtotime('now'));
        $model->save();
        $id = $model->getTestId();
        $model = Mage::getModel('test/emp');
        $model->setEmpPost($data['post']);
        $model->setTestId($id);
        $model->save();
        $this->_redirectReferer(Mage::getUrl('*/*'));

    }

    public function loadAction()
    {
        $id = $_POST["row_id"];
        $data = $this->getRequest()->getPost();
        $model = Mage::getModel('test/test')->load($id)->addData($data);
        try {
            $model->setId($id)->save();
            echo $this->__('Data updated successfully.');
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        $model = Mage::getModel('test/emp')->load($id)->addData($data);
        try {
            $model->setId($id)->save();
            echo $this->__('Data updated emp successfully.');
        } catch (Exception $e) {
            echo $e->getMessage();
        }

    }
    public function deleteAction()
    {
        $test_id = $_POST["id"];
        $model = Mage::getModel('test/test')->load($test_id);
        $model->delete();
    }
    protected function _beforeSave()
    {
        parent::_beforeSave();
        if ($this->getResource()->checkDuplicate($this)) {
            throw new Exception("Error Processing Request");

        }
        return $this;
    }
}
