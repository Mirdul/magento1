
<?php
$installer = $this;
$installer->startSetup();
$installer->run("

-- DROP TABLE IF EXISTS {$this->getTable('emp')};
CREATE TABLE {$this->gettable('emp')} (
  `emp_id` int(11) unsigned NOT NULL auto_increment,
  `emp_post` varchar(225) NOT NULL default '',
  `test_id` int(11) unsigned NOT NULL default '0' ,
  PRIMARY KEY(`emp_id`),
  FOREIGN KEY (`test_id`) REFERENCES {$this->getTable('test')} (`test_id`) ON DELETE CASCADE ON UPDATE CASCADE
)ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
$installer->endSetup();

?>