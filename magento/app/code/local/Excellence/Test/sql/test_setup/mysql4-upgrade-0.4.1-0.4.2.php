<?php
$installer = $this;
$installer->startSetup();
$installer->run("

-- DROP TABLE IF EXISTS {$this->getTable('contact')};
CREATE TABLE {$this->gettable('contact')} (
  `user_id` int(11) unsigned NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  `email` varchar(255) NOT NULL default '',
  `comment` text,
  PRIMARY KEY  (`user_id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
$installer->endSetup();
