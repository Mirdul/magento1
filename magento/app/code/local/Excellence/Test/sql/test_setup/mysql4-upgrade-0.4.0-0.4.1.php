<?php
$installer = $this;
$installer->startSetup();
$installer->run("

-- DROP TABLE IF EXISTS {$this->getTable('user')};
CREATE TABLE {$this->gettable('user')} (
  `customer_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  `login_time` datetime NULL,
  `logout_time` datetime NULL,
  UNIQUE KEY  (`user_id`),
  PRIMARY KEY (`customer_id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
$installer->endSetup();
?>