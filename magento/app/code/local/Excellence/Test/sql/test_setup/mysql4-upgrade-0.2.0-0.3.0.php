<?php
$installer = $this;
$installer->startSetup();
$installer->run("

-- DROP TABLE IF EXISTS {$this->getTable('user')};
CREATE TABLE {$this->gettable('user')} (
  `user_id` int(11) unsigned NOT NULL auto_increment,
  `lname` varchar(255) NOT NULL default '',
  `login_time` datetime NULL,
  `logout_time` datetime NULL,
  PRIMARY KEY (`user_id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
$installer->endSetup();

?>