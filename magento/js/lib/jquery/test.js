(function ($) {
    $(document).ready(function () {
        $(document).on('click', '.edit', function (event) {
            event.preventDefault();
            var tbl_row = $(this).closest('tr');
            var row_id = tbl_row.attr('id');
            tbl_row.find('.save').show();
            tbl_row.find('.edit').hide();
            tbl_row.find('.row_data')
                .attr('contenteditable', 'true')
                .css('background-color', '#ccd893')
                .css('padding', '3px')
            tbl_row.find('.row_data').each(function (index, value) {
                $(this).attr('original_entry', $(this).html());
            });

        });
        
    });
})(jQuery);