(function ($) {
    $(document).ready(function () {
        $(document).on('click', '.edit', function (event) {
            event.preventDefault();
            var tbl_row = $(this).closest('tr');
            var row_id = tbl_row.attr('id');
            tbl_row.find('.save').show();
            tbl_row.find('.edit').hide();
            tbl_row.find('.row_data')
                .attr('contenteditable', 'true')
                .css('background-color', '#ccd893')
                .css('padding', '3px')
            tbl_row.find('.row_data').each(function (index, value) {
                $(this).attr('original_entry', $(this).html());
            });

        });


        $('.toggle').click(function (e) {
            e.preventDefault();
            var $this = $(this);
            if ($this.next().hasClass('show')) {
                $this.next().removeClass('show');
                $this.next().slideUp(350);
                $this.children(".plusminus").text('+');

            } else {
                $this.parent().parent().find('li .inner').removeClass('show');
                $this.parent().parent().find('.plusminus').text('+');
                $this.parent().parent().find('li .inner').slideUp(350);
                $this.next().toggleClass('show');
                $this.next().slideToggle(350);
                $this.children(".plusminus").text('-');

            }
        });
    });
    
})(jQuery);